# Generated by Django 2.0.9 on 2019-02-04 17:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('choco', '0005_courses'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice_List',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_amt', models.CharField(max_length=10)),
                ('invc_date', models.DateTimeField(auto_now=True)),
                ('invc_id', models.CharField(max_length=20, unique=True)),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='course_inc', to='choco.Courses')),
                ('lead', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lead_invoice_list', to='choco.Lead_List')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_invoice_list', to='choco.User_Details')),
            ],
        ),
    ]
